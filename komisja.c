/*
 * PIOTR DOMANSKI
 * NUMER INDEKSU: 346901
 * DRUGIE ZADANIE ZALICZENIOWE Z SO
 * PROGRAM KOMISJA
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdbool.h>
#include "wspolne.h"
#include "err.h"

Liczba NUMER_KOMISJI;
Liczba ILOSC_UPRAWNIONYCH;
Liczba ILOSC_WSZYSTKICH_GLOSOW;
Liczba identyfikatorKolejki;
Liczba identyfikatorKolejkiKomisji;
Liczba identyfikatorKolejkiOdbieraniaPotwiedzeniaOdSerwera;
Liczba NUMER_LISTY;
Liczba NUMER_KANDYDATA;
Liczba ILOSC_GLOSOW;
Liczba ILOSC_GLOSOW_WAZNYCH = 0;
Liczba ILOSC_PRZETWORZONYCH_WPISOW = 0;
struct Wiadomosc zadanie;
struct Wiadomosc przyznanieDostepu;
struct Wiadomosc czescWynikowWyborow;
struct Wiadomosc raportZSerwera;

void inicjujKolejke() {
	identyfikatorKolejki = msgget(KLUCZ_KOLEJKI, 0);
	identyfikatorKolejkiKomisji = msgget(KLUCZ_KOLEJKI_WYSYLANIA_WYNIKOW, 0);
	identyfikatorKolejkiOdbieraniaPotwiedzeniaOdSerwera =
		msgget(KLUCZ_KOLEJKI_WYSYLANIA_KOMUNIKATU_KONCOWEGO_KOMISJI, 0);
	if (identyfikatorKolejki == -1 || identyfikatorKolejkiKomisji == -1
		|| identyfikatorKolejkiOdbieraniaPotwiedzeniaOdSerwera == -1) {
		syserr("BLAD TWORZENIA KOLEJEK");
	}
}

bool czyMogeWyslacWynikiNaSerwer() {
	tworzZeraWiodace(&NUMER_KOMISJI, 5);
	Liczba pid = getpid();
	tworzZeraWiodace(&pid, 5);
	sprintf(zadanie.zawartoscWiadomosci, "K %d %d", NUMER_KOMISJI, pid);
	pid -= STO_TYSIECY;
	Liczba iloscZnakow = strlen(zadanie.zawartoscWiadomosci);
	zadanie.typWiadomosci = IDENTYFIKATOR_SERWERA;
	Liczba blad = msgsnd(identyfikatorKolejki, (char*) (&zadanie), iloscZnakow, 0);
	if (blad != 0)
		syserr("BLAD WYSYLANIA ZADANIA");
	blad = msgrcv(identyfikatorKolejki, &przyznanieDostepu,
		MAX_MESSAGE_DATA, (long long) (pid), 0L);
	if (blad == -1)
		syserr("BLAD ODBIERANIA ODPOWIEDZI");
	//	printf("ODPOWIEDZ: %s\n", przyznanieDostepu.zawartoscWiadomosci);
	return (przyznanieDostepu.zawartoscWiadomosci[0] == 'T');
}

const Liczba PIERWSZY_WIERSZ_DANYCH_WEJSCIOWYCH = 0;
const Liczba WYNIK_KANDYDATA = 1;
const Liczba INFORMACJA_O_KONCU_DANYCH_WEJSCIOWYCH = 2;

void wyslijDaneNaSerwer(Liczba coWyslac) {
	switch (coWyslac) {
		case 0:
			sprintf(czescWynikowWyborow.zawartoscWiadomosci, "%d %d",
				ILOSC_UPRAWNIONYCH, ILOSC_WSZYSTKICH_GLOSOW);
			break;
		case 1:
			sprintf(czescWynikowWyborow.zawartoscWiadomosci, "%d %d %d",
				NUMER_LISTY, NUMER_KANDYDATA, ILOSC_GLOSOW);
			break;
		case 2:
			sprintf(czescWynikowWyborow.zawartoscWiadomosci, "KONIEC");
			break;
		default:
			return;
	}
	//	printf("%s\n", czescWynikowWyborow.zawartoscWiadomosci);
	Liczba iloscZnakow = strlen(czescWynikowWyborow.zawartoscWiadomosci);
	Liczba pid = getpid();
	czescWynikowWyborow.typWiadomosci = pid;
	Liczba blad = msgsnd(identyfikatorKolejkiKomisji,
		(char*) (&czescWynikowWyborow), iloscZnakow, 0);
	if (blad != 0)
		syserr("BLAD WYSYLANIA WYNIKOW NA SERWER");
}

void zakonczWysylanieDanych() {
	wyslijDaneNaSerwer(INFORMACJA_O_KONCU_DANYCH_WEJSCIOWYCH);
	//	printf("WYSLALEM DANE NA SERWER\n");
	ILOSC_UPRAWNIONYCH -= STO_TYSIECY;
	ILOSC_WSZYSTKICH_GLOSOW -= STO_TYSIECY;
}

void czytajWynikiZWejscia() {
	scanf("%d %d", &ILOSC_UPRAWNIONYCH, &ILOSC_WSZYSTKICH_GLOSOW);
	tworzZeraWiodace(&ILOSC_UPRAWNIONYCH, 5);
	tworzZeraWiodace(&ILOSC_WSZYSTKICH_GLOSOW, 5);
	//TODO PAMIETAJ O PRZYWROCENIU WARTOSCI PO WYSLANIU
	wyslijDaneNaSerwer(PIERWSZY_WIERSZ_DANYCH_WEJSCIOWYCH);
	Liczba ileWczytanych = 0;
	Liczba wczytywana = 0;
	while (scanf("%d", &wczytywana) == 1) {
		switch (ileWczytanych) {
			case 0:
				NUMER_LISTY = wczytywana;
				break;
			case 1:
				NUMER_KANDYDATA = wczytywana;
				break;
			case 2:
				ILOSC_GLOSOW = wczytywana;
				tworzZeraWiodace(&NUMER_LISTY, 3);
				tworzZeraWiodace(&NUMER_KANDYDATA, 3);
				tworzZeraWiodace(&ILOSC_GLOSOW, 5);
				wyslijDaneNaSerwer(WYNIK_KANDYDATA);
				break;
		}
		ileWczytanych = (ileWczytanych + 1) % 3;
	}
	zakonczWysylanieDanych();
}

void odbierzPotwierdzenieZSerwera() {
	msgrcv(identyfikatorKolejkiOdbieraniaPotwiedzeniaOdSerwera,
		&raportZSerwera, MAX_MESSAGE_DATA, (long long) (getpid()), 0L);
	ILOSC_GLOSOW_WAZNYCH = zwrocLiczbe(raportZSerwera.zawartoscWiadomosci, 0, 9);
	ILOSC_GLOSOW_WAZNYCH -= MILIARD;
	ILOSC_PRZETWORZONYCH_WPISOW = zwrocLiczbe(raportZSerwera.zawartoscWiadomosci, 11, 16);
	ILOSC_PRZETWORZONYCH_WPISOW -= STO_TYSIECY;
}

void wypiszKomunikatKoncowy() {
	printf("Przetworzonych wpisów: %d\n", ILOSC_PRZETWORZONYCH_WPISOW);
	printf("Uprawnionych do głosowania: %d\n", ILOSC_UPRAWNIONYCH);
	printf("Głosów ważnych: %d\n", ILOSC_GLOSOW_WAZNYCH);
	printf("Głosów nieważnych: %d\n", ILOSC_WSZYSTKICH_GLOSOW - ILOSC_GLOSOW_WAZNYCH);
	double FREKWENCJA = (ILOSC_WSZYSTKICH_GLOSOW) * 100;
	FREKWENCJA /= ILOSC_UPRAWNIONYCH;
	//TODO SENSOWNE POKAZYWANIE FREKWENCJI
	printf("Frekwencja w lokalu: %lf\n", FREKWENCJA);
}

int main(int argc, char** argv) {
	NUMER_KOMISJI = atoi(argv[1]);
	signal(SIGQUIT, zakonczWysylanieDanych);
	inicjujKolejke();
	if (czyMogeWyslacWynikiNaSerwer()) {
		//		printf("Dostep przyznany\n");
		czytajWynikiZWejscia();
		odbierzPotwierdzenieZSerwera();
		wypiszKomunikatKoncowy();
	} else {
		printf("Odmowa dostepu\n");
	}
	return (EXIT_SUCCESS);
}