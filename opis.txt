PIOTR DOMAŃSKI
NUMER INDEKSU: 346901
DRUGIE ZADANIE ZALICZENIOWE Z SO
OPIS PROTOKOLÓW KOMUNIKACYJNYCH

1. Specyficznie przesyłanie liczb.
W moim programie wszystkie liczby mają STAŁĄ długość po zapisaniu do bufora. 
Jest tak dla tego, że przed przesłaniem dodaje do nich pewną potęgę dziesiątki 
(funkcja tworzZeraWiodace z wspolne.h), a po odebraniu odejmuje ją.
Dzięki temu każdy program wie na jakich indeksach w buforze znajdują 
się interesujące go liczby. Do wyciągnięcia ich z bufora używam funckji zwrocLiczbe
z wspolne.h.

2. Opis kolejek komunikatów:
a)	
	Kolejka oznaczona identyfikatorem KLUCZ_KOLEJKI sluży do wysyłania
	informacji od klientów typu raport lub komisja do serwera.

	Raport wysyła komunikat postaci:
	R NUMER_LISTY (jeśli nieokreślone to 999) PID. Są one oczywiście stałej długości.
	Następnie raport nie korzysta już z tej kolejki.

	Komisja wysyła komunikat postaci:
	K NUMER_KOMISJI PID. Następnie oczekuje na tej kolejce używając swojego
	pid-u na odpowiedź od serwera. Odpowiedź ma postać:
	T - jeżeli ta komisja nie wysyłała wsześniej wyników.
	N - ta komisja wysyłała wsześniej wyniki.
	
	Kiedy klient wyśle SIGINT serwerowi ta kolejka zostanie zamknięta.
	Żadne nowe żądania nie będą przetwarzane, jednakże dotychczasowe
	zostaną dokończone.

b)	
	Kolejka oznaczona identyfikatorem KLUCZ_KOLEJKI_WYSYLANIA_WYNIKOW
	sluży do wysyłania wyników od komisji do dynamicznie utworzonego wątku
	przez serwer. Wątek ten czeka na komunikaty na swoim pidzie.
	Komisja wysyła wszystko co otrzymała na wejściu. Liczby są sformatowane. 
	Ostatnim komunikatem jaki wysyła jest KONIEC.

c)
	Kolejka oznaczona identyfikatorem KLUCZ_KOLEJKI_WYSYLANIA_RAPORTOW
	sluży do wysyłania raportu od dynamicznie utworzonego wątku
	do programu raport. Wysyła ona zawartość KOPII wyników wyborów 
	reprezentowaną jako struktura Wyniki. Ze względów wydajnościowych
	wysyłane są tylko wyniki realnie istniejących kandydatów
	(tzn do L i K ,a nie do MAX_L oraz MAX_K).

d)	
	Kolejka oznaczona identyfikatorem KLUCZ_KOLEJKI_WYSYLANIA_KOMUNIKATU_
		KONCOWEGO_KOMISJI sluży do wysyłania raportu od dynamicznie utworzonego wątku
	do programu komisja. Zawiera on liczbę przetworzonych wpisów i sumę głosów.

3. Synchronizacja rwlock dla komisji i raportów.

W przypadku komisji(pisarza) sytuacja wygląda następująco.
Dynamicznie utworzony wątek wczytuje wszystkie dane od komisji.
Zapamiętuje je w strukturze DaneOdKomisji. Następnie stara się dostać do sekcji
krytycznej ,gdzie aktualizuje te wartości.
Aktualizacja jest bardzo szybka. Jest to kilka pętli i pojedyńczych istrukcji.
Przy zachowaniu takiej taktyki wiele komisji ,może pracować jednocześnie, choć
tylko jedna może bardzo szybko aktualizować wyniki wyborów w tym samym momencie.

W przypadku raportu(czytelnika) jego sekcja krytyczna zawiera jedynie kopiowanie
wyników wybórów. Jest również szybka i nie ma możliwości wystąpienia błędów.
Po wyjściu z sekcji stopniowo wysyła on zawartość kopii do programu raport.

4. Ograniczenie ilości klientów do 100.
Dzięki temu nie ma zbyt dużego zużycia pamięci. Jeżeli liczba wątków jest zbyt duża
to główny wątek serwera wisi na semaforze ograniczenieGorneIlosciWatkow.
Wątki utworzone zmniejszają licznik aktualnie działających wątków i jeżeli serwer
czeka to zostaje zwolniony.

5. Po otrzymaniu SIGINTA serwer dokończy zaczęte zadania ,ale nie rozpocznie nowych.