FLAGS = -Wall -std=c99
IPC_FLAGS = -D_XOPEN_SOURCE=600

all: serwer raport komisja

serwer: serwer.c err.o
	gcc $(CFLAGS) $(IPC_FLAGS) -pthread serwer.c err.o -o serwer

raport: raport.c err.o
	gcc $(CFLAGS) $(IPC_FLAGS) raport.c err.o -o raport

komisja: komisja.c err.o
	gcc $(CFLAGS) $(IPC_FLAGS) komisja.c err.o -o komisja

err.o: err.c err.h
	gcc $(CFLAGS) -c err.c -o err.o

clean:
	rm -rf *.o
