/*
 * PIOTR DOMANSKI
 * NUMER INDEKSU: 346901
 * DRUGIE ZADANIE ZALICZENIOWE Z SO
 * PROGRAM RAPORT
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdbool.h> 
#include "wspolne.h"
#include "err.h"

Liczba NUMER_LISTY = 999;
Liczba identyfikatorKolejki;
Liczba identyfikatorKolejkiOdbieraniaRaportu;
Liczba pid;

Liczba przetworzonychKomisji;
Liczba wszystkichKomisji;
Liczba uprawnionychDoGlosowania;
Liczba glosowWaznych;
Liczba glosowNiewaznych;
Liczba ileList;
Liczba ileKandydatow;

struct Wiadomosc zadanie;
struct Wiadomosc zawartoscRaportu;
struct Wyniki pobraneWyniki;

void inicjujZmienne() {
	pid = getpid();
	Liczba i, j;
	zawartoscRaportu.typWiadomosci = pid;
	zadanie.typWiadomosci = IDENTYFIKATOR_SERWERA;
	for (i = 0; i < MAX_L; i++) {
		for (j = 0; j < MAX_K; j++) {
			pobraneWyniki.wynikiKandydatow[i][j] = 0;
		}
	}
}

void wypiszStatystyki() {
	printf("Przetworzonych komisji: %d / %d\n", przetworzonychKomisji,
		wszystkichKomisji);
	printf("Uprawnionych do głosowania: %d\n", uprawnionychDoGlosowania);
	printf("Głosów ważnych: %d\n", glosowWaznych);
	printf("Głosów nieważnych: %d\n", glosowNiewaznych);
	double frekwencja = 0;
	if (uprawnionychDoGlosowania != 0) {
		frekwencja = 100 * (glosowWaznych + glosowNiewaznych);
		frekwencja /= uprawnionychDoGlosowania;
	}
	printf("Frekwencja : %lf\n", frekwencja);
}

void wypiszPojedynczaListe(Liczba numerListy) {
	Liczba sumaGlosowNaTeListe = 0;
	Liczba i;
	for (i = 0; i < ileKandydatow; i++) {
		sumaGlosowNaTeListe += pobraneWyniki.wynikiKandydatow[numerListy][i];
	}
	printf("%d %d ", numerListy + 1, sumaGlosowNaTeListe);
	for (i = 0; i < ileKandydatow; i++) {
		printf("%d ", pobraneWyniki.wynikiKandydatow[numerListy][i]);
	}
	printf("\n");
}

void inicjujKolejke() {
	identyfikatorKolejki = msgget(KLUCZ_KOLEJKI, 0);
	identyfikatorKolejkiOdbieraniaRaportu = msgget(
		KLUCZ_KOLEJKI_WYSYLANIA_RAPORTOW, 0);
	if (identyfikatorKolejki == -1 ||
		identyfikatorKolejkiOdbieraniaRaportu == -1)
		syserr("BLAD INICJALIZACJI KOLEJEK");
}

void wyslijZadanie() {
	tworzZeraWiodace(&NUMER_LISTY, 5);
	tworzZeraWiodace(&pid, 5);
	sprintf(zadanie.zawartoscWiadomosci, "R %d %d", NUMER_LISTY, pid);
	NUMER_LISTY -= STO_TYSIECY;
	pid -= STO_TYSIECY;
	Liczba iloscZnakow = strlen(zadanie.zawartoscWiadomosci);
	zadanie.typWiadomosci = IDENTYFIKATOR_SERWERA;
	Liczba blad = msgsnd(identyfikatorKolejki, (char*) (&zadanie), iloscZnakow, 0);
	if (blad != 0)
		syserr("BLAD WYSYLANIA ZADANIA");
}

void wczytajStatystyki() {
	Liczba blad = msgrcv(identyfikatorKolejkiOdbieraniaRaportu,
		&zawartoscRaportu, MAX_MESSAGE_DATA, pid, 0);
	if (blad == -1)
		syserr("BLAD ODBIERANIA STATYSTYK");
	//	printf("%s\n", zawartoscRaportu.zawartoscWiadomosci);
	glosowNiewaznych = zwrocLiczbe(zawartoscRaportu.zawartoscWiadomosci,
		0, 9);
	glosowNiewaznych -= MILIARD;
	glosowWaznych = zwrocLiczbe(zawartoscRaportu.zawartoscWiadomosci,
		11, 20);
	glosowWaznych -= MILIARD;
	przetworzonychKomisji = zwrocLiczbe(zawartoscRaportu.zawartoscWiadomosci,
		22, 31);
	przetworzonychKomisji -= MILIARD;
	uprawnionychDoGlosowania = zwrocLiczbe(zawartoscRaportu.zawartoscWiadomosci,
		33, 42);
	uprawnionychDoGlosowania -= MILIARD;
	wszystkichKomisji = zwrocLiczbe(zawartoscRaportu.zawartoscWiadomosci,
		44, 53);
	wszystkichKomisji -= MILIARD;
	ileList = zwrocLiczbe(zawartoscRaportu.zawartoscWiadomosci,
		55, 64);
	ileList -= MILIARD;
	ileKandydatow = zwrocLiczbe(zawartoscRaportu.zawartoscWiadomosci,
		66, 75);
	ileKandydatow -= MILIARD;
}

void wczytajWynikKandydata(Liczba numerListy, Liczba numerKandydata) {
	Liczba blad = msgrcv(identyfikatorKolejkiOdbieraniaRaportu,
		&zawartoscRaportu, MAX_MESSAGE_DATA, pid, 0);
	if (blad == -1)
		syserr("BLAD ODIBERANIA WYNIKU KANDYDATA");
	//	printf("%s\n", zawartoscRaportu.zawartoscWiadomosci);
	pobraneWyniki.wynikiKandydatow[numerListy][numerKandydata] =
		zwrocLiczbe(zawartoscRaportu.zawartoscWiadomosci, 0, 9);
	pobraneWyniki.wynikiKandydatow[numerListy][numerKandydata] -= MILIARD;
	//	printf("POBRALEM %d %d\n", numerListy, numerKandydata);
}

void wczytajDane() {
	wczytajStatystyki();
	Liczba i, j;
	if (NUMER_LISTY == 999) {
		for (i = 0; i < ileList; i++) {
			for (j = 0; j < ileKandydatow; j++) {
				wczytajWynikKandydata(i, j);
			}
		}
	} else {
		for (j = 0; j < ileKandydatow; j++) {
			wczytajWynikKandydata(NUMER_LISTY - 1, j);
		}
	}
}

void wypiszRaport() {
	wypiszStatystyki();
	Liczba i;
	if (NUMER_LISTY == 999) {
		for (i = 0; i < ileList; i++) {
			wypiszPojedynczaListe(i);
		}
	} else {
		wypiszPojedynczaListe(NUMER_LISTY - 1);
	}
}

int main(int argc, char** argv) {
	inicjujZmienne();
	if (argc == 2) {
		NUMER_LISTY = atoi(argv[1]);
	}
	inicjujKolejke();
	wyslijZadanie();
	wczytajDane();
	wypiszRaport();
	return (EXIT_SUCCESS);
}
