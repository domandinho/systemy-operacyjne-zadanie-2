/*
 * PIOTR DOMANSKI
 * NUMER INDEKSU: 346901
 * DRUGIE ZADANIE ZALICZENIOWE Z SO
 * DEFINICJE STALYCH FUNCJI I STRUKTUR
 */
#ifndef WSPOLNE_H
#define	WSPOLNE_H

#ifdef	__cplusplus
extern "C" {
#endif
#define	MAX_MESSAGE_DATA 4000

	struct Wiadomosc {
		long typWiadomosci;
		char zawartoscWiadomosci[MAX_MESSAGE_DATA];
	};

	typedef int Liczba;
	const Liczba KLUCZ_KOLEJKI = 1235;
	const Liczba KLUCZ_KOLEJKI_WYSYLANIA_WYNIKOW = 1236;
	const Liczba KLUCZ_KOLEJKI_WYSYLANIA_RAPORTOW = 1237;
	const Liczba KLUCZ_KOLEJKI_WYSYLANIA_KOMUNIKATU_KONCOWEGO_KOMISJI = 1238;
	const Liczba IDENTYFIKATOR_SERWERA = 98765;
	const Liczba MAX_L = 100;
	const Liczba MAX_K = 100;
	const Liczba MAX_M = 10000;
	const Liczba TYSIAC = 1000;
	const Liczba STO_TYSIECY = 100000;
	const Liczba MILIARD = 1000000000;
	const Liczba MAKSYMALNA_LICZBA_OBSLUGIWANYCH_KLIENTOW = 100;

	struct Wyniki {
		Liczba iloscList;
		Liczba iloscKandydatow;
		Liczba iloscPrzetworzonychKomisji;
		Liczba iloscWszystkichKomisji;
		Liczba iloscUprawnionychDoGlosowania;
		Liczba iloscGlosowWaznych;
		Liczba iloscGlosowNiewaznych;
		Liczba wynikiKandydatow[100][100];
	};

	void tworzZeraWiodace(Liczba* formatowana, Liczba ileZer) {
		Liczba bufor = 1;
		while (ileZer) {
			bufor *= 10;
			ileZer--;
		}
		*formatowana += bufor;
	}

	Liczba zwrocLiczbe(char* bufor, Liczba poczatkowyIndex, Liczba koncowyIndex) {
		Liczba i, potega10 = 1, wynik = 0;
		for (i = koncowyIndex; i >= poczatkowyIndex; i--) {
			wynik += potega10 * (bufor[i] - '0');
			potega10 *= 10;
		}
		return wynik;
	}

#ifdef	__cplusplus
}
#endif

#endif
