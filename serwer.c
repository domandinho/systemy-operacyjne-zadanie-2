/*
 * PIOTR DOMANSKI
 * NUMER INDEKSU: 346901
 * DRUGIE ZADANIE ZALICZENIOWE Z SO
 * PROGRAM SERWER
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdbool.h>
#include "wspolne.h"
#include <pthread.h>
#include "err.h"

struct DaneOdKomisji {
	Liczba liczbaUprawnionychDoGlosowania;
	Liczba liczbaOsobKtoreOddalyGlosWDanymLokalu;
	Liczba iloscWierszy;
	Liczba numerListy[10000];
	Liczba numerKandydata[10000];
	Liczba iloscGlosow[10000];
};

struct DaneORaporcie {
	Liczba pid;
	Liczba numerListy;
};

/*
 * ZMIENNE WSPOLDZIELONE PRZEZ WATKI ALE RAZ WYZNACZONE
 * PRZY URUCHAMIANIU SERWERA NIE ULEGAJA MODYFIKACJI
 */
Liczba ILOSC_LIST;
Liczba ILOSC_KANDYDATOW;
Liczba ILOSC_KOMISJI;
Liczba identyfikatorKolejki;
Liczba identyfikatorKolejkiKomisji;
Liczba identyfikatorKolejkiWysylaniaRaportu;
Liczba identyfikatorKolejkiWysylaniaPotwierdzenKomisji;
pthread_attr_t atrybutJoinable;

pthread_rwlock_t blokadaZmianyWynikowWyborow;
pthread_mutex_t ochrona;
pthread_mutex_t ograniczenieGorneIlosciWatkow;

/*
 * ZMIENNE Z KTORYCH KORZYSTA TYLKO GLOWNY WATEK PROGRAMU
 */
bool czyKomisjaSieZglosila[10000];
pthread_t watkiKlientow[100];
struct Wiadomosc zadaniaWysylaneNaSerwer;
struct Wiadomosc informacjaODostepieDlaKomisji;
struct Wiadomosc informacjaODostepieDlaRaportu;

/*
 * WSPOLDZIELONE WYNIKI WYBOROW
 * DOSTEP WYMAGA SYNCHRONIZACJI
 */
struct Wyniki wspoldzieloneWyniki;
/*
 * ZMIENNE DO KTORYCH DOSTEP WYMAGA SYNCHRONIZACJI
 */
Liczba iloscObslugiwanychKlientow;
bool czySerwerCzeka;
Liczba pidyKlientow[100];

//FUNKCJA WYKONYWANA TYLKO JEDEN RAZ PRZEZ GLOWNY WATEK SERWERA

void inicjalizacjaZmiennych() {
	czySerwerCzeka = false;
	Liczba blad;
	blad = pthread_attr_init(&atrybutJoinable);
	if (blad != 0)
		syserr("BLAD INICJALIZACJI ARTYBUTU");
	blad = pthread_rwlock_init(&blokadaZmianyWynikowWyborow, NULL);
	if (blad != 0)
		syserr("BLAD INICJALIZACJI RWLOCK");
	blad = pthread_mutex_init(&ochrona, 0);
	if (blad != 0)
		syserr("BLAD INICJALIZACJI MUTEXA OCHRONA");
	blad = pthread_mutex_init(&ograniczenieGorneIlosciWatkow, 0);
	if (blad != 0)
		syserr("BLAD INICJALIZACJI MUTEXA OGRANICZENIE");
	blad = pthread_mutex_lock(&ograniczenieGorneIlosciWatkow);
	if (blad != 0)
		syserr("BLAD OPUSZCZANIA MUTEXA OGRANICZENIE");
	//USTAWIENIE NA 0.
	int i, j;
	for (i = 0; i < MAX_M; i++) {
		czyKomisjaSieZglosila[i] = false;
	}
	wspoldzieloneWyniki.iloscList = ILOSC_LIST;
	wspoldzieloneWyniki.iloscKandydatow = ILOSC_KANDYDATOW;
	wspoldzieloneWyniki.iloscGlosowNiewaznych = 0;
	wspoldzieloneWyniki.iloscGlosowWaznych = 0;
	wspoldzieloneWyniki.iloscPrzetworzonychKomisji = 0;
	wspoldzieloneWyniki.iloscUprawnionychDoGlosowania = 0;
	wspoldzieloneWyniki.iloscWszystkichKomisji = ILOSC_KOMISJI;
	for (i = 0; i < MAX_L; i++) {
		for (j = 0; j < MAX_K; j++) {
			wspoldzieloneWyniki.wynikiKandydatow[i][j] = 0;
		}
	}
	iloscObslugiwanychKlientow = 0;
	for (i = 0; i < MAKSYMALNA_LICZBA_OBSLUGIWANYCH_KLIENTOW; i++) {
		pidyKlientow[i] = -1;
	}
}

/*
 * FUNKCJA WYKONYWANA TYLKO JEDEN RAZ PRZEZ GLOWNY WATEK SERWERA
 */
void inicjalizacjaKolejek() {
	identyfikatorKolejki = msgget(
		KLUCZ_KOLEJKI, 0666 | IPC_CREAT | IPC_EXCL);
	identyfikatorKolejkiKomisji = msgget(KLUCZ_KOLEJKI_WYSYLANIA_WYNIKOW,
		0666 | IPC_CREAT | IPC_EXCL);
	identyfikatorKolejkiWysylaniaRaportu = msgget(
		KLUCZ_KOLEJKI_WYSYLANIA_RAPORTOW, 0666 | IPC_CREAT | IPC_EXCL);
	identyfikatorKolejkiWysylaniaPotwierdzenKomisji = msgget(
		KLUCZ_KOLEJKI_WYSYLANIA_KOMUNIKATU_KONCOWEGO_KOMISJI, 0666 | IPC_CREAT | IPC_EXCL);
	if (identyfikatorKolejki == -1 || identyfikatorKolejkiKomisji == -1
		|| identyfikatorKolejkiWysylaniaRaportu == -1
		|| identyfikatorKolejkiWysylaniaPotwierdzenKomisji == -1) {
		syserr("BLAD TWORZENIA KOLEJEK KOMUNIKATOW");
	}
}

/* 
 * FUNKCJA WYKONYWANA PRZEZ DYNAMICZNE WATKI. 
 * JEST OSTATNIA FUNKCJA WYKONANA PRZEZ WATEK PRZED ZAKONCZENIEM
 */
void poinformujSerwerOSwoimZakonczeniu(Liczba pid) {
	Liczba blad = 1;
	blad = pthread_mutex_lock(&ochrona);
	if (blad != 0)
		syserr("BLAD OPUSZCZANIA MUTEXA OCHRONA");
	{
		if (czySerwerCzeka) {
			czySerwerCzeka = false;
			blad = pthread_mutex_unlock(&ograniczenieGorneIlosciWatkow);
			if (blad != 0)
				syserr("BLAD PODNOSZENIA MUTEXA OGRANICZENIA");
		}
		iloscObslugiwanychKlientow--;
		Liczba i;
		for (i = 0; i < MAKSYMALNA_LICZBA_OBSLUGIWANYCH_KLIENTOW; i++) {
			if (pidyKlientow[i] == pid) {
				pidyKlientow[i] = -1;
				break;
			}
		}
	}
	blad = pthread_mutex_unlock(&ochrona);
	if (blad != 0)
		syserr("BLAD PODNOSZENIA MUTEXA OCHRONA");
}

/* 
 * FUNKCJA WYKONYWANA PRZEZ GLOWNY WATEK SERWERA. 
 * BLOKUJE GO JESLI WATKOW JEST ZA DUZO
 */
void czekajNaPrzetworzenieNowychKlientow() {
	bool czyCzekac = false;
	Liczba blad = 1;
	blad = pthread_mutex_lock(&ochrona);
	if (blad != 0)
		syserr("BLAD OPUSZCZANIA MUTEXA OCHRONA");
	{
		if (iloscObslugiwanychKlientow ==
			MAKSYMALNA_LICZBA_OBSLUGIWANYCH_KLIENTOW) {
			czyCzekac = true;
			czySerwerCzeka = true;
		}
	}
	blad = pthread_mutex_unlock(&ochrona);
	if (blad != 0)
		syserr("BLAD PODNOSZENIA MUTEXA OCHRONA");
	if (czyCzekac == true) {
		blad = pthread_mutex_lock(&ograniczenieGorneIlosciWatkow);
		if (blad != 0) {
			syserr("BLAD OPUSZCZANIA MUTEXA OGRANICZENIA");
		}
	}
}

/* 
 * FUNKCJA WYKONYWANA PRZEZ GLOWNY WATEK SERWERA. 
 * AKTUALIZUJE TABLICE PIDOW KLIENTOW. 
 * ZAKLADA ZE MOZNA UTWORZYC NOWY WATEK
 */
Liczba wpiszWatekDoTablicy(Liczba pid) {
	Liczba i, indeksWTablicyWatkow;
	Liczba blad = 1;
	blad = pthread_mutex_lock(&ochrona);
	if (blad != 0)
		syserr("BLAD OPUSZCZANIA MUTEXA OCHRONA");
	{
		iloscObslugiwanychKlientow++;
		for (i = 0; i < MAKSYMALNA_LICZBA_OBSLUGIWANYCH_KLIENTOW; i++) {
			if (pidyKlientow[i] == -1) {
				pidyKlientow[i] = pid;
				indeksWTablicyWatkow = i;
				break;
			}
		}
	}
	blad = pthread_mutex_unlock(&ochrona);
	if (blad != 0)
		syserr("BLAD PODNOSZENIA MUTEXA OCHRONA");
	return indeksWTablicyWatkow;
}

void zwolnijKolejkiIMechanizmySynchronizacyjne() {
	Liczba blad;
	blad = msgctl(identyfikatorKolejkiKomisji, IPC_RMID, 0);
	if (blad != 0)
		syserr("BLAD ZAMYKANIA KOLEJKI ODBIERANIA WYNIKOW");
	blad = msgctl(identyfikatorKolejkiWysylaniaRaportu, IPC_RMID, 0);
	if (blad != 0)
		syserr("BLAD ZAMYKANIA KOLEJKI WYSYLANIA RAPORTOW");
	blad = msgctl(identyfikatorKolejkiWysylaniaPotwierdzenKomisji, IPC_RMID, 0);
	if (blad != 0)
		syserr("BLAD ZAMYKANIA KOLEJKI WYSYLANIA POTWIERZEN");
	blad = pthread_rwlock_destroy(&blokadaZmianyWynikowWyborow);
	if (blad != 0)
		syserr("BLAD NISZCZENIA BLOKADY RWLOCK");
	blad = pthread_mutex_destroy(&ochrona);
	if (blad != 0)
		syserr("BLAD NISZCZENIA MUTEXA OCHRONA");
	blad = pthread_mutex_unlock(&ograniczenieGorneIlosciWatkow);
	if (blad != 0)
		syserr("BLAD PODNOSZENIA MUTEXA OGRANICZENIA");
	blad = pthread_mutex_destroy(&ograniczenieGorneIlosciWatkow);
	if (blad != 0)
		syserr("BLAD NISZCZENIA MUTEXA OGRANICZENIA");
}

/*
 * FUNKCJA OBSLUGI SIGINT
 */
void zamknijSerwer(int signal) {
	Liczba blad = 1;
	blad = msgctl(identyfikatorKolejki, IPC_RMID, 0);
	if (blad != 0)
		syserr("BLAD ZAMYKANIA KOLEJKI KOMUNIKATOW");
	printf("KONIEC PRZYJMOWANIA NOWYCH ZGLOSZEN\n");
	Liczba i;
	for (i = 0; i < MAKSYMALNA_LICZBA_OBSLUGIWANYCH_KLIENTOW; i++) {
		if (pidyKlientow[i] != -1) {
			blad = pthread_join(watkiKlientow[i], NULL);
			if (blad != 0)
				syserr("BLAD OPERACJI JOIN");
		}
	}
	zwolnijKolejkiIMechanizmySynchronizacyjne();
	printf("KONIEC PRACY SERWERA\n");
	exit(0);
}

//POCZATEK FUNKCJI OBSLUGI RAPORTU ORAZ POMOCNICZYCH

/*
 * FUNKCJA WYKONYWANA PRZEZ DYNAMICZNY WATEK OBSLUGUJACY PROGRAM RAPORT
 * FUNKCJA WYMAGA SYNCHRONIZACJI RWLOCK BLOKADA READLOCK
 */
void kopiujWynikiWyborow(struct Wyniki* kopia) {
	kopia->iloscList = wspoldzieloneWyniki.iloscList;
	kopia->iloscKandydatow = wspoldzieloneWyniki.iloscKandydatow;
	kopia->iloscGlosowNiewaznych = wspoldzieloneWyniki.iloscGlosowNiewaznych;
	kopia->iloscGlosowWaznych = wspoldzieloneWyniki.iloscGlosowWaznych;
	kopia->iloscPrzetworzonychKomisji = wspoldzieloneWyniki.iloscPrzetworzonychKomisji;
	kopia->iloscUprawnionychDoGlosowania = wspoldzieloneWyniki.iloscUprawnionychDoGlosowania;
	kopia->iloscWszystkichKomisji = wspoldzieloneWyniki.iloscWszystkichKomisji;
	Liczba i, j;
	for (i = 0; i < MAX_L; i++) {
		for (j = 0; j < MAX_K; j++) {
			kopia->wynikiKandydatow[i][j] =
				wspoldzieloneWyniki.wynikiKandydatow[i][j];
		}
	}
}

void wyslijPorcjeDanychDotyczacaRaportu(struct Wiadomosc* czescRaportu) {
	Liczba iloscZnakow = strlen(czescRaportu->zawartoscWiadomosci);
	Liczba blad = 1;
	blad = msgsnd(identyfikatorKolejkiWysylaniaRaportu, (char*) (czescRaportu),
		iloscZnakow, 0);
	if (blad != 0)
		syserr("BLAD WYSYLANIA RAPORTU");
}

void wyslijStatystyki(struct Wyniki* kopia, struct Wiadomosc* czescRaportu) {
	tworzZeraWiodace(&(kopia->iloscGlosowNiewaznych), 9);
	tworzZeraWiodace(&(kopia->iloscGlosowWaznych), 9);
	tworzZeraWiodace(&(kopia->iloscPrzetworzonychKomisji), 9);
	tworzZeraWiodace(&(kopia->iloscUprawnionychDoGlosowania), 9);
	tworzZeraWiodace(&(kopia->iloscWszystkichKomisji), 9);
	tworzZeraWiodace(&(kopia->iloscList), 9);
	tworzZeraWiodace(&(kopia->iloscKandydatow), 9);
	sprintf(czescRaportu->zawartoscWiadomosci, "%d %d %d %d %d %d %d",
		kopia->iloscGlosowNiewaznych, kopia->iloscGlosowWaznych,
		kopia->iloscPrzetworzonychKomisji, kopia->iloscUprawnionychDoGlosowania,
		kopia->iloscWszystkichKomisji, kopia->iloscList,
		kopia->iloscKandydatow);
	wyslijPorcjeDanychDotyczacaRaportu(czescRaportu);
}

void wyslijWynikKonkretnegoKandydata(struct Wyniki* kopia, struct Wiadomosc* czescRaportu,
	Liczba numerListyKandydata, Liczba numerAktualnegoKandydata) {
	tworzZeraWiodace(&(kopia->wynikiKandydatow
		[numerListyKandydata][numerAktualnegoKandydata]), 9);
	sprintf(czescRaportu->zawartoscWiadomosci, "%d",
		kopia->wynikiKandydatow[numerListyKandydata][numerAktualnegoKandydata]);
	wyslijPorcjeDanychDotyczacaRaportu(czescRaportu);
}

/*
 * FUNKCJA BEDACA PARAMETREM TWORZENIA WATKU OBSLUGUJACEGO PROGRAM RAPORT
 */
void* wyslijRaport(void* dane) {
	struct DaneORaporcie raport = *((struct DaneORaporcie*) (dane));
	struct Wyniki kopia;
	struct Wiadomosc czescRaportu;
	czescRaportu.typWiadomosci = raport.pid;
	Liczba blad = 1;
	blad = pthread_rwlock_rdlock(&blokadaZmianyWynikowWyborow);
	if (blad != 0)
		syserr("BLAD URUCHAMIANIA BLOKADY READ LOCK");
	{
		kopiujWynikiWyborow(&kopia);
	}
	blad = pthread_rwlock_unlock(&blokadaZmianyWynikowWyborow);
	if (blad != 0)
		syserr("BLAD WYLACZANIA BLOKADY READ LOCK");
	Liczba i, j;
	Liczba ileList = kopia.iloscList;
	Liczba ileKandydatow = kopia.iloscKandydatow;
	wyslijStatystyki(&kopia, &czescRaportu);
	for (i = 0; i < ileList; i++) {
		if (i == raport.numerListy - 1 || raport.numerListy == 999) {
			for (j = 0; j < ileKandydatow; j++) {
				wyslijWynikKonkretnegoKandydata(&kopia, &czescRaportu, i, j);
			}
		}
	}
	poinformujSerwerOSwoimZakonczeniu(raport.pid);
}

/*
 * FUNKCJA WYWOLYWANA PRZEZ GLOWNY WATEK SERWERA
 */
void tworzWatekWysylajacyRaport(Liczba pidRaportu, Liczba numerListy,
	Liczba indeksWTablicyWatkow) {
	Liczba blad = 1;
	struct DaneORaporcie dane;
	dane.pid = pidRaportu;
	dane.numerListy = numerListy;
	blad = pthread_create(&watkiKlientow[indeksWTablicyWatkow], &atrybutJoinable,
		wyslijRaport, (void*) (&dane));
	if (blad != 0)
		syserr("BLAD TWORZENIA WATKU");
}

/*
 * FUNKCJA WYWOLYWANA PRZEZ GLOWNY WATEK SERWERA
 */
void parsujRaport() {
	Liczba numerListy = zwrocLiczbe(zadaniaWysylaneNaSerwer.zawartoscWiadomosci,
		2, 7) - STO_TYSIECY;
	Liczba pidOdbiorcy = zwrocLiczbe(zadaniaWysylaneNaSerwer.zawartoscWiadomosci,
		9, 14);
	pidOdbiorcy -= STO_TYSIECY;
	Liczba indeksWTablicyWatkow = wpiszWatekDoTablicy(pidOdbiorcy);
	tworzWatekWysylajacyRaport(pidOdbiorcy, numerListy, indeksWTablicyWatkow);
}

//POCZATEK FUNKCJI OBSLUGI KOMISJI ORAZ POMOCNICZYCH

void parsujPierwszyWierszWynikowOdKomisji(Liczba* pid,
	struct DaneOdKomisji *odebrane,
	struct Wiadomosc *czescWynikowWyborow) {
	Liczba blad = -1;
	blad = msgrcv(identyfikatorKolejkiKomisji, czescWynikowWyborow,
		MAX_MESSAGE_DATA, (long long) (*pid), 0L);
	if (blad == -1)
		syserr("BLAD ODCZYTU WIADOMOSCI OD KOMISJI");
	odebrane->liczbaUprawnionychDoGlosowania = zwrocLiczbe(
		czescWynikowWyborow->zawartoscWiadomosci, 0, 5);
	odebrane->liczbaUprawnionychDoGlosowania -= STO_TYSIECY;
	odebrane->liczbaOsobKtoreOddalyGlosWDanymLokalu = zwrocLiczbe(
		czescWynikowWyborow->zawartoscWiadomosci, 7, 12);
	odebrane->liczbaOsobKtoreOddalyGlosWDanymLokalu -= STO_TYSIECY;
}

bool parsujWynikKandydataIOdpowiedzCzyToOstatni(Liczba* pid,
	struct DaneOdKomisji *odebrane,
	struct Wiadomosc *czescWynikowWyborow) {
	Liczba blad = -1;
	blad = msgrcv(identyfikatorKolejkiKomisji, czescWynikowWyborow,
		MAX_MESSAGE_DATA, (long long) (*pid), 0L);
	if (blad == -1)
		syserr("BLAD ODCZYTU WYNIKOW OD KOMISJI");
	if (czescWynikowWyborow->zawartoscWiadomosci[0] == 'K') {
		return true;
	}
	odebrane->numerListy[odebrane->iloscWierszy] =
		zwrocLiczbe(czescWynikowWyborow->zawartoscWiadomosci, 0, 3);
	odebrane->numerListy[odebrane->iloscWierszy] -= TYSIAC;
	odebrane->numerKandydata[odebrane->iloscWierszy] =
		zwrocLiczbe(czescWynikowWyborow->zawartoscWiadomosci, 5, 8);
	odebrane->numerKandydata[odebrane->iloscWierszy] -= TYSIAC;
	odebrane->iloscGlosow[odebrane->iloscWierszy] =
		zwrocLiczbe(czescWynikowWyborow->zawartoscWiadomosci, 10, 15);
	odebrane->iloscGlosow[odebrane->iloscWierszy] -= STO_TYSIECY;
	odebrane->iloscWierszy++;
	return false;
}

/*
 * FUNKCJA WYKONYWANA PRZEZ WATEK OBSLUGUJACY KOMISJE
 * FUNKCJA WYMAGA SYNCHRONIZACJI RWLOCK BLOKADA WRITE LOCK
 */
void aktualizujAktualnyStanWyborow(struct DaneOdKomisji* dane) {
	wspoldzieloneWyniki.iloscUprawnionychDoGlosowania +=
		dane->liczbaUprawnionychDoGlosowania;
	wspoldzieloneWyniki.iloscPrzetworzonychKomisji++;
	Liczba sumaGlosow = 0;
	Liczba i = 0;
	for (i = 0; i < dane->iloscWierszy; i++) {
		sumaGlosow += dane->iloscGlosow[i];
		wspoldzieloneWyniki.wynikiKandydatow
			[dane->numerListy[i] - 1][dane->numerKandydata[i] - 1] +=
			dane->iloscGlosow[i];
	}
	wspoldzieloneWyniki.iloscGlosowWaznych += sumaGlosow;
	wspoldzieloneWyniki.iloscGlosowNiewaznych +=
		(dane->liczbaOsobKtoreOddalyGlosWDanymLokalu - sumaGlosow);
}

void wyslijInformacjeKoncowaDoKomisji(Liczba* pid,
	struct DaneOdKomisji* odebrane) {
	struct Wiadomosc koncowaInformacjaDlaKomisji;
	Liczba sumaGlosow = 0;
	Liczba iloscPrzetworzonychWpisow = odebrane->iloscWierszy;
	int i;
	for (i = 0; i < odebrane->iloscWierszy; i++) {
		sumaGlosow += odebrane->iloscGlosow[i];
	}
	tworzZeraWiodace(&sumaGlosow, 9);
	tworzZeraWiodace(&iloscPrzetworzonychWpisow, 5);
	koncowaInformacjaDlaKomisji.typWiadomosci = *pid;
	sprintf(koncowaInformacjaDlaKomisji.zawartoscWiadomosci, "%d %d",
		sumaGlosow, iloscPrzetworzonychWpisow);
	Liczba liczbaZnakow = strlen(koncowaInformacjaDlaKomisji.zawartoscWiadomosci);
	Liczba blad = msgsnd(identyfikatorKolejkiWysylaniaPotwierdzenKomisji,
		(char*) (&koncowaInformacjaDlaKomisji), liczbaZnakow, 0);
	if (blad != 0)
		syserr("BLAD WYSYLANIA INFORMACJI KONCOWEJ DO KOMISJI");
}

/*
 * FUNKCJA BEDACA PARAMETREM TWORZENIA WATKU OBSLUGUJACEGO KOMISJE
 */
void *odbierzWynikiOdKomisji(void *pidKomisji) {
	Liczba pid = *((Liczba*) (pidKomisji));
	Liczba blad = 1;
	struct DaneOdKomisji odebrane;
	struct Wiadomosc czescWynikowWyborow;
	odebrane.iloscWierszy = 0;
	parsujPierwszyWierszWynikowOdKomisji(&pid, &odebrane, &czescWynikowWyborow);
	while (1 == 1) {
		if (parsujWynikKandydataIOdpowiedzCzyToOstatni(&pid, &odebrane,
			&czescWynikowWyborow)) {
			break;
		}
	}
	blad = pthread_rwlock_wrlock(&blokadaZmianyWynikowWyborow);
	if (blad != 0)
		syserr("BLAD URUCHAMIANIA BLOKADY WRITELOCK");
	{
		aktualizujAktualnyStanWyborow(&odebrane);
	}
	blad = pthread_rwlock_unlock(&blokadaZmianyWynikowWyborow);
	if (blad != 0)
		syserr("BLAD WYLACZANIA BLOKADY WRITELOCK");
	wyslijInformacjeKoncowaDoKomisji(&pid, &odebrane);
	poinformujSerwerOSwoimZakonczeniu(pid);
	return (void*) (0);
}

void tworzWatekOdbierajacyWynikiOdKomisji(Liczba pidKomisji,
	Liczba indeksWTablicyWatkow) {
	pthread_t watekCzytajacyWyniki;
	Liczba blad = pthread_create(&watkiKlientow[indeksWTablicyWatkow], &atrybutJoinable,
		odbierzWynikiOdKomisji, (void*) (&pidKomisji));
	if (blad != 0)
		syserr("BLAD TWORZENIA WATKU ODBIERAJACEGO WYNIKI OD KOMISJI");
}

/*
 * FUNKCJA WYKONYWANA PRZEZ GLOWNY WATEK PROGRAMU.
 * PARSUJE PIERWSZY KOMUNIKAT NADESLANY PRZEZ KOMISJE.
 */
void parsujKomisje() {
	Liczba i;
	Liczba numerKomisji = zwrocLiczbe(zadaniaWysylaneNaSerwer.zawartoscWiadomosci,
		2, 7) - 100000;
	Liczba pidOdbiorcy = zwrocLiczbe(zadaniaWysylaneNaSerwer.zawartoscWiadomosci,
		9, 14);
	pidOdbiorcy -= STO_TYSIECY;
	if (czyKomisjaSieZglosila[numerKomisji]) {
		sprintf(informacjaODostepieDlaKomisji.zawartoscWiadomosci, "N");
	} else {
		sprintf(informacjaODostepieDlaKomisji.zawartoscWiadomosci, "T");
	}
	czyKomisjaSieZglosila[numerKomisji] = true;
	Liczba liczbaZnakow = strlen(informacjaODostepieDlaKomisji.zawartoscWiadomosci);
	informacjaODostepieDlaKomisji.typWiadomosci = pidOdbiorcy;
	Liczba blad = msgsnd(identyfikatorKolejki, (char*) (&informacjaODostepieDlaKomisji),
		liczbaZnakow, 0);
	if (blad != 0)
		syserr("BLAD WYSYLANIA INFORMACJI O DOSTEPIE DO KOMISJI");
	if (informacjaODostepieDlaKomisji.zawartoscWiadomosci[0] == 'T') {
		Liczba indeksWTablicyWatkow = wpiszWatekDoTablicy(pidOdbiorcy);
		tworzWatekOdbierajacyWynikiOdKomisji(pidOdbiorcy,
			indeksWTablicyWatkow);
	}
}

/*
 * TA FUNKCJA DZIALA W PETLI NIESKONCZONEJ KTORA MOZE PRZERWAC TYLKO SIGINT
 */
void przetwarzajZapytania() {
	czekajNaPrzetworzenieNowychKlientow();
	Liczba blad = msgrcv(identyfikatorKolejki, &zadaniaWysylaneNaSerwer,
		MAX_MESSAGE_DATA, (long long) (IDENTYFIKATOR_SERWERA), 0L);
	if (blad == -1)
		syserr("BLAD WCZYTYWANIA ZADANIA");
	if (zadaniaWysylaneNaSerwer.zawartoscWiadomosci[0] == 'R') {
		parsujRaport();
	} else {
		parsujKomisje();
	}
}

int main(int argc, char** argv) {
	ILOSC_LIST = atoi(argv[1]);
	ILOSC_KANDYDATOW = atoi(argv[2]);
	ILOSC_KOMISJI = atoi(argv[3]);
	inicjalizacjaZmiennych();
	inicjalizacjaKolejek();
	if (signal(SIGINT, zamknijSerwer) == SIG_ERR) {
		syserr("BLAD REJESTRACJI FUNKCJI OBSLUGI SIGINT");
	}
	while (1 == 1) {
		przetwarzajZapytania();
	}
	return (EXIT_SUCCESS);
}